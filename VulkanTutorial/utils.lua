function StrEndWith(str, suffix)
    return string.sub(str,-string.len(suffix))==suffix
end

function CurPlat()
-- windows
-- cross
-- linux
-- macosx
-- android
-- iphoneos
-- watchos
    if is_plat("windows") then
        return "windows"
    elseif is_plat("cross") then
        return "cross"
    elseif is_plat("linux") then
        return "linux"
    elseif is_plat("macosx") then
        return "macosx"
    else
        return "others"
    end 
end