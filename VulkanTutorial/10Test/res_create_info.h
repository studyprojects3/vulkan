#ifndef RES_CREATE_INFO

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <vulkan/vulkan.h>

struct TextureCreateInfo
{
	const char* debugName = nullptr;
	glm::vec2 dimensions;
	std::vector<char> initialData;
};

struct ShaderCreateInfo
{
	const char* debugName = nullptr;
	std::vector<char> byteCode;
	std::string entryFunc;
};

struct GraphicsStateCreateInfo
{
	// vertex input

	// input assembly
	VkPrimitiveTopology topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	VkBool32 primitiveRestartEnable = VK_FALSE;

	// viewports and scissors
	VkViewport viewport = {
		.x = 0.0f, .y = 0.0f,
		.minDepth = 0.0f, .maxDepth = 1.0f,
	};
	VkRect2D scissor = {
		.offset = {0, 0},
		.extent = {-1,-1},
	};

	// rasterizer
	VkPolygonMode polygonMode = VK_POLYGON_MODE_FILL;
	float lineWidth = 1.0f;
	VkCullModeFlagBits cullMode = VK_CULL_MODE_NONE;
	VkFrontFace frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	VkBool32 depthBiasEnable = VK_FALSE;

	// multisampling
	VkBool32 sampleShadingEnable = VK_FALSE;
	VkBool32 alphaToCoverageEnable = VK_FALSE;

	// depth and stencil
	VkBool32 depthTestEnable = VK_TRUE;
	VkBool32 depthWriteEnable = VK_TRUE;
	VkCompareOp depthCompareOp = VK_COMPARE_OP_LESS;
};

struct PipelineCreateInfo
{
	const char* debugName = nullptr;
	ShaderCreateInfo VS;
	ShaderCreateInfo PS;

	GraphicsStateCreateInfo graphicsState;
};

class ResMgr
{
public:
	static VkPipeline CreatePipeline();
};
#endif // !RES_CREATE_INFO
