#ifndef MISC_HEADER

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <optional>
#include <vector>
#include <string>

struct QueueFamilyIndices 
{
    // uint32_t 可以表示的任何一个值都可能是一个可用的 queue family index std::optional<uint32_t> 来表示 queue family index
    // 支持图形绘制命令的 QueueFamily index
    std::optional<uint32_t> graphicsFamily;
    // 支持展示绘制结果的 QueueFamily index
    std::optional<uint32_t> presentFamily;

    bool isComplete() 
    {
		// 当 graphicsFamily.has_value() 为 false 或 presentFamily.has_value() 为 false 时，表示不是一个可用的 queue family
        return graphicsFamily.has_value() && presentFamily.has_value();
    }
};

struct SwapChainSupportDetails 
{
    VkSurfaceCapabilitiesKHR capabilities;
    std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> presentModes;
};

extern std::vector<char> readFile(const std::string& filename);
#endif // !MISC_HEADER
