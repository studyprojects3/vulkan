#include "misc.h"
#include <vector>
#include <iostream>
#include <fstream>

std::vector<char> readFile(const std::string& filename) 
{
    // std::ios::ate 从文件最后开始读，方便获取文件大小
    // std::ios::binary 以二进制新式读取文件，避免文本转换
    std::ifstream file(filename, std::ios::ate | std::ios::binary);

    if (!file.is_open()) 
    {
        std::cout << "file : " << filename << std::endl;
        throw std::runtime_error("failed to open file!");
    }

    // 获取文件大小
    size_t fileSize = (size_t) file.tellg();
	std::vector<char> buffer(fileSize);
    // 回到文件起始位置，再开始读取文件
	file.seekg(0);
	file.read(buffer.data(), fileSize);
    file.close();
    return buffer;
}

