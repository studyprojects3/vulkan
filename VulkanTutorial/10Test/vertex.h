#include <glm/glm.hpp>
#include <vulkan/vulkan.h>
#include <array>
#include <vector>

struct Vertex 
{
    glm::vec2 pos;
    glm::vec3 color;

    static VkVertexInputBindingDescription getBindingDescription()
    {
        // 定义 顶点输入 绑定描述。其描述了以什么样的rate从内存加载数据（顶点属性地址是一个顶点索引的函数，还是一个实例索引的函数）
        VkVertexInputBindingDescription bindingDescription{};
        // 指定当前binding在bindings数组中的索引值
		bindingDescription.binding = 0;
        // 指定一个数据项的步长(stride) 以字节为单位 即一个数据项到下一个数据项有多少个字节
		bindingDescription.stride = sizeof(Vertex);
        // 指定顶点属性地址是一个顶点索引的函数
		bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        return bindingDescription;
    }

    static std::array<VkVertexInputAttributeDescription, 2> getAttributeDescriptions() 
    {
		std::array<VkVertexInputAttributeDescription, 2> attributeDescriptions{};
        // 指定每个顶点数据来自哪个 binding
        attributeDescriptions[0].binding = 0;
        // 和 vertex shader中的location 指令相对应
		attributeDescriptions[0].location = 0;
        // 指定该属性对应数据的类型
		attributeDescriptions[0].format = VK_FORMAT_R32G32_SFLOAT;
        // 指定该属性在一个数据项中的偏移
		attributeDescriptions[0].offset = offsetof(Vertex, pos);

        attributeDescriptions[1].binding = 0;
		attributeDescriptions[1].location = 1;
		attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[1].offset = offsetof(Vertex, color);

		return attributeDescriptions;
	}
};

const std::vector<Vertex> quad_vertices = 
{
    {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
    {{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
    {{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
    {{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}},
};

const std::vector<uint16_t> quad_indices = {
    0, 1, 2, 2, 3, 0
};

