add_rules("mode.debug", "mode.release")

rule("vulkan_tutorial")
    print(os.projectdir())
    if is_plat("windows") then
        --add_defines("ENGINE_ROOT_DIR=\"" .. (os.projectdir():gsub("\\", "\\\\")) .. "\\\\engine\"")
        print("platform is windows")
    else 
        print("platform is others")
        --add_defines("ENGINE_ROOT_DIR=\"" .. (os.projectdir():gsub("\\", "/")) .. "/engine\"")
    end

    before_run(function (target)
        print("[shader] glsl to spirv..")
        import("utils")
        local vulkan_sdk = find_package("vulkansdk") --定位到vulkansdk的路径
        local glslang_validator_dir = vulkan_sdk["bindir"].."\\glslangValidator.exe" --获取到glslangValidator.exe的路径
        -- 遍历 shader 文件
        local targetName = target:name()
        for _, shader_path in ipairs(os.files("$(projectdir)/"..targetName.."/**")) do
            if utils.StrEndWith(shader_path, ".vert") or utils.StrEndWith(shader_path, ".frag") then
                local new_shader_path = string.gsub(shader_path, ".vert", ".vert.spv")
                new_shader_path = string.gsub(new_shader_path, ".frag", ".frag.spv")
                os.runv(glslang_validator_dir,{"-V", shader_path,"-o", new_shader_path}) --执行系统命令
                print("[shader] done: "..shader_path)
            end
        end
    end)
rule_end()

-- add_requires("vulkansdk", {debug=true})
-- add_requires("vulkansdk > 1.3.0", {debug=true})
set_languages("c++20")
add_requires("vulkansdk")
add_requires("glm")
add_requires("glfw")
add_requires("stb")
add_requires("tinyobjloader")

local targetName = "01DevelopmentEnvironment"
target(targetName)
    set_kind("binary")
    add_files(targetName .. "/*.cpp")
    add_packages("vulkansdk", "glm", "glfw")

targetName = "02DrawATriangle"
target(targetName)
    set_kind("binary")
    add_files(targetName .. "/*.cpp")
    -- 设置 build output directory
    --set_targetdir("../build/"..targetName)
    -- 设置 run directory (也就是工作目录)
    set_rundir("$(projectdir)/"..targetName)
    add_packages("vulkansdk", "glm", "glfw")
    add_rules("vulkan_tutorial")

targetName = "03VertexBuffer"
target(targetName)
    set_kind("binary")
    add_files(targetName .. "/*.cpp")
    set_rundir("$(projectdir)/"..targetName)
    add_packages("vulkansdk", "glm", "glfw")
    add_rules("vulkan_tutorial")

targetName = "09Multisampling"
target(targetName)
    set_kind("binary")
    add_files(targetName .. "/*.cpp")
    set_rundir("$(projectdir)/"..targetName)
    add_packages("vulkansdk", "glm", "glfw", "stb", "tinyobjloader")
    add_rules("vulkan_tutorial")

targetName = "10Test"
target(targetName)
    set_kind("binary")
    add_files(targetName .. "/*.cpp")
    add_headerfiles(targetName .. "/*.h")
    add_extrafiles(targetName .. "/*.vert", targetName .. "/*.frag", targetName .. "/*.spv")
    -- 设置 build output directory
    --set_targetdir("../build/"..targetName)
    -- 设置 run directory (也就是工作目录)
    set_rundir("$(projectdir)/"..targetName)
    add_packages("vulkansdk", "glm", "glfw", "stb", "tinyobjloader")
    add_rules("vulkan_tutorial")
    set_default(true)


--
-- If you want to known more usage about xmake, please see https://xmake.io
--
-- ## FAQ
--
-- You can enter the project directory firstly before building project.
--
--   $ cd projectdir
--
-- 1. How to build project?
--
--   $ xmake
--
-- 2. How to configure project?
--
--   $ xmake f -p [macosx|linux|iphoneos ..] -a [x86_64|i386|arm64 ..] -m [debug|release]
--
-- 3. Where is the build output directory?
--
--   The default output directory is `./build` and you can configure the output directory.
--
--   $ xmake f -o outputdir
--   $ xmake
--
-- 4. How to run and debug target after building project?
--
--   $ xmake run [targetname]
--   $ xmake run -d [targetname]
--
-- 5. How to install target to the system directory or other output directory?
--
--   $ xmake install
--   $ xmake install -o installdir
--
-- 6. Add some frequently-used compilation flags in xmake.lua
--
-- @code
--    -- add debug and release modes
--    add_rules("mode.debug", "mode.release")
--
--    -- add macro definition
--    add_defines("NDEBUG", "_GNU_SOURCE=1")
--
--    -- set warning all as error
--    set_warnings("all", "error")
--
--    -- set language: c99, c++11
--    set_languages("c99", "c++11")
--
--    -- set optimization: none, faster, fastest, smallest
--    set_optimize("fastest")
--
--    -- add include search directories
--    add_includedirs("/usr/include", "/usr/local/include")
--
--    -- add link libraries and search directories
--    add_links("tbox")
--    add_linkdirs("/usr/local/lib", "/usr/lib")
--
--    -- add system link libraries
--    add_syslinks("z", "pthread")
--
--    -- add compilation and link flags
--    add_cxflags("-stdnolib", "-fno-strict-aliasing")
--    add_ldflags("-L/usr/local/lib", "-lpthread", {force = true})
--
-- @endcode
--

